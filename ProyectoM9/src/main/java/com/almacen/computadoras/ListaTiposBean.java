package com.almacen.computadoras;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

@Named
@SessionScoped
public class ListaTiposBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 484041659215795615L;
	
	private List<TipoModel> tipos;
	
	public ListaTiposBean() {
		// TODO Auto-generated constructor stub
		System.out.println("Constructor");
		this.tipos = new ArrayList<TipoModel>();
	}

	public List<TipoModel> getTipos() {
		return tipos;
	}

	public void setTipos(List<TipoModel> tipos) {
		this.tipos = tipos;
	}

	public void agregar(TipoModel tiposBean) {
		
		tipos.add(tiposBean);
		
	}



}
