package com.almacen.computadoras;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.inject.Model;
import jakarta.inject.Named;

@Named
@SessionScoped
public class ListaOrdenesBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7603519532439693412L;

	private List<OrdenModel> ordenes;

	public ListaOrdenesBean() {
		// TODO Auto-generated constructor stub
		System.out.println("Constructor");
		this.ordenes = new ArrayList<OrdenModel>();
	}

	public List<OrdenModel> getOrdenes() {
		return ordenes;
	}

	public void setOrdenes(List<OrdenModel> ordenes) {
		this.ordenes = ordenes;
	}

	public void agregar(OrdenModel ordenesBean) {
		
		ordenes.add(ordenesBean);
		
	}

}
