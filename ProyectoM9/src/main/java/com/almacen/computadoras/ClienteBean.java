package com.almacen.computadoras;

import java.io.Serializable;

import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.inject.Model;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
@SessionScoped
public class ClienteBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7141020465980742077L;

	@Inject
	private MessageBean messageBean;

	private String nombre;
	private String direccion;
	private String telefono;
	private String rfc;
	private String nacimiento;



	public String registrar() {
		System.out.println(nombre);
		System.out.println(direccion);
		System.out.println(telefono);
		System.out.println(rfc);
		System.out.println(nacimiento);
		messageBean.setMensaje("El cliente con "+direccion+" ha quedado registrado!!");
		return "index";
	}



	public MessageBean getMessageBean() {
		return messageBean;
	}



	public void setMessageBean(MessageBean messageBean) {
		this.messageBean = messageBean;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getDireccion() {
		return direccion;
	}



	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}



	public String getTelefono() {
		return telefono;
	}



	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	public String getRfc() {
		return rfc;
	}



	public void setRfc(String rfc) {
		this.rfc = rfc;
	}



	public String getNacimiento() {
		return nacimiento;
	}



	public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
	}



	@Override
	public String toString() {
		return "ClienteBean [messageBean=" + messageBean + ", nombre=" + nombre + ", direccion=" + direccion
				+ ", telefono=" + telefono + ", rfc=" + rfc + ", nacimiento=" + nacimiento + "]";
	}



}
