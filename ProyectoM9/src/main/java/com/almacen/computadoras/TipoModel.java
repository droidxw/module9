package com.almacen.computadoras;

import java.io.Serializable;

import jakarta.enterprise.inject.Model;
import jakarta.inject.Inject;

@Model
//serializable para persistir al usar session en N lugares BD, memoria, contexto
//cuando es un request solo existe la info del bean de clase a clase
public class TipoModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8463063443349736411L;

		
	private String fabricante;
	private String clave;

	private String tipoEquipo;
	private Integer existencias;
	
	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	

	public String getTipoEquipo() {
		return tipoEquipo;
	}

	public void setTipoEquipo(String tipoEquipo) {
		this.tipoEquipo = tipoEquipo;
	}
	
	public String getClave() {
		return clave;
	}

	public void setClave(String descripcion) {
		this.clave = descripcion;
	}
	
	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

	@Override
	public String toString() {
		return "TipoModel [fabricante=" + fabricante + ", tipoEquipo=" + tipoEquipo + ", clave=" + clave 
				+ ", existencias=" + existencias + "]";
	}



}
