package com.almacen.computadoras;

import java.io.Serializable;

import jakarta.enterprise.inject.Model;
import jakarta.inject.Inject;

@Model
public class CobroBean implements Serializable{

	


	/**
	 * 
	 */
	private static final long serialVersionUID = -1714705383594884489L;
	
	@Inject
	private MessageBean messageBean;
	
	private String cantidad;
	private String fecha;
	private String cuenta;
	private String metodo;
	
	
	public String registrar() {
		System.out.println(cantidad);
		System.out.println(fecha);
		System.out.println(cuenta);
		System.out.println(metodo);
		
		messageBean.setMensaje("Tu cuenta es: "+cuenta+" MXN");
		return "index";
	}
	
	public String getCantidad() {
		return cantidad;
	}


	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getCuenta() {
		return cuenta;
	}


	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}


	public String getMetodo() {
		return metodo;
	}


	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
}
